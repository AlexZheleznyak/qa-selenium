package by.career.itstep;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class TestMainPage {

    WebDriver driver;
    MainPage page;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver",
                "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        page = new MainPage(driver);
    }

    @AfterClass
    public void shutDown() {
        driver.close();
    }

    @Test
    public void test() {
        page.open();

        assertFalse(page.isConsultationModalOpen());
        assertTrue(page.atPage());
    }

    @Test
    public void testConsultationModal() throws Exception {
        page.open();
        page.openConsultationModal();

        Thread.sleep(1000); // ждем 1 секунду

        assertTrue(page.isConsultationModalOpen());
    }

    @Test
    public void testFillConsultationModal_happyPath() throws Exception {
        page.open();
        page.openConsultationModal();

        assertTrue(page.isConsultationModalOpen());

        page.fillConsultationForm("Alex", "Xela", true, "QA3819");

        Thread.sleep(5000);
    }

}







